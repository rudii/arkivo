Ползвах [тази статия](https://aleksandarjakovljevic.com/convert-pdf-images-using-imagemagick/)
По нея, въвеждайки командата:
convert -density 150 -antialias "input_file_name.pdf" -append -resize 1024x -quality 100 "output_file_name.png"

получавах отговор от системата си (Arco Linux):
convert: attempt to perform an operation not allowed by the security policy `gs' @ error/delegate.c/ExternalDelegateCommand/378.

Намерих тази статия в [stackoverflow](), в която открих препратка към [тази страница в arch wiki](https://wiki.archlinux.org/title/ImageMagick), по която се водих, за да закоментирам реда <policy domain="delegate" rights="none" pattern="gs" /> във файла /etc/ImageMagick-7/policy.xml

С това командата от статията проработи и събра всичките страници в pdf файла на една от матурите в едно единствено, много високо png изображение

След това промених параметрите на командата, за да стане по-подходяща за целите на тази конкретна задача. Документацията за всички тях е в [сайта на ImageMagic](https://imagemagick.org/script/command-line-options.php)
convert -density 600 "input_file_name.pdf" -append -quality 100 "output_file_name.png"

Гъстота 600 се оказа прекалено много, защото RAM паметта ми 8GB (и swap дяла, който е още толкова) се препълниха и процесът беше прекъснат автоматично от системата.

convert -density 300 "input_file_name.pdf" -append -quality 100 "output_file_name.png"

Това проработи.
След това премахнах преходните пиксели (antialiasing), като добавих +antialias преди името на входния файл. Внимание! Ако го добавя след името на входния файл, преливащите пиксели се появяват. Кодът се получи:
convert -density 300 +antialias 2020-06-17.pdf -append -quality 100 out.png

След това пробвах да премахна параметъра за качество, защото png изображенията се компресират без загуби на качеството и този параметър всъщност контролира степента на компресиране. Също премахнах алфа канала и зададох на програмата да оптимизира палитрата на изходното изображение и да изведе всяка страница в отделен файл, вместо да ги събира в един общ.

convert -density 300 -alpha remove +antialias -type optimize 2018-05-23.pdf out2.png

Това ми позволи да се върна на резолюция от 600dpi без да ми източи цялата RAM памет, защото премахването на излишните канали и разделянето на отделни файлове направи тази резолюция възможна за параметрите на компютъра ми.

convert -density 600 -alpha remove +antialias -type optimize 2018-05-23.pdf out4.png

Резултатът ми харесва.


 2018-05-23.pdf out4.png
find . -name "*.pdf" -exec 

for i in {1..5}; do COMMAND-HERE; done

for i in *.pdf; do convert -density 100 -alpha remove +antialias -type optimize "$i" "$.png"; done
for i in *.pdf
do
convert -density 100 -alpha remove +antialias -type optimize "$i" "$i.png"
done

или 
FOR %G IN (*.png) DO convert %G -channel A -threshold 254 batch\%G